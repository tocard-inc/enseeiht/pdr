package linda.search.basic;

import java.util.UUID;
import java.util.stream.Stream;

import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import linda.Callback;
import linda.Linda;
import linda.Tuple;
import linda.Linda.eventMode;
import linda.Linda.eventTiming;
import linda.server.LindaClient;

public class Manager implements Runnable {

    private Linda linda;

    private UUID reqUUID;
    private String pathname;
    private String search;
    private int bestvalue = Integer.MAX_VALUE; // lower is better
    private String bestresult;
    private Integer timeout = null;

    public Manager(Linda linda, String pathname, String search) {
        this.linda = linda;
        this.pathname = pathname;
        this.search = search;
        this.reqUUID = UUID.randomUUID();
        this.search = search;
    }

    public Manager(Linda linda, String pathname, String search, int timeout) {
        this(linda, pathname, search);
        this.timeout = timeout;
    }

    public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException {
        if (args.length != 3) {
            System.err.println("linda.search.basic.Main search file.");
            return;
        }
        int port = Integer.parseInt(args[0]);
        String url = "rmi://localhost:" + port + "/linda";
        LindaClient lc = new LindaClient(url);

        String dict = args[1];
        String word = args[2];

        Manager manager = new Manager(lc, dict, word);
        Thread thread = new Thread(manager);
        thread.start();
    }

    private void addSearch() {
        System.out.println("Search (" + this.reqUUID + ") for " + this.search);
        linda.eventRegister(
                Linda.eventMode.TAKE, Linda.eventTiming.FUTURE,
                new Tuple(Code.Result, this.reqUUID, String.class, Integer.class),
                new CbUpdate());
        linda.write(new Tuple(Code.Request, this.reqUUID, this.search));
    }

    private void loadData() {
        try (Stream<String> stream = Files.lines(Paths.get(pathname))) {
            stream.limit(10000).forEach(s -> linda.write(new Tuple(Code.Value, s.trim(), this.reqUUID)));
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    class CbLoop implements Callback {

        eventMode mode;
        eventTiming timing;
        Tuple template;

        CbLoop(eventMode mode, eventTiming timing, Tuple template) {
            this.mode = mode;
            this.timing = timing;
            this.template = template;

            linda.eventRegister(this.mode, this.timing, this.template, this);
        }

        public void call(Tuple t) {
            linda.eventRegister(this.mode, this.timing, this.template, this);
        }
    }

    private class CbEnd implements Callback {
        @Override
        public void call(Tuple t) {
            new CbLoop(eventMode.TAKE, eventTiming.IMMEDIATE, t); // on supprime tous les futurs "done"
            linda.take(new Tuple(Code.Request, reqUUID, String.class)); // remove search query
            System.out.println("query done");
        }
    }

    private class CbUpdate implements Callback {
        public void call(Tuple t) { // [ Result, ?UUID, ?String, ?Integer ]
            UUID reqUUID = (UUID) t.get(1);
            String s = (String) t.get(2);
            Integer v = (Integer) t.get(3);

            if (v < bestvalue) {
                bestvalue = v;
                bestresult = s;
                System.out.println("New best (" + bestvalue + "): \"" + bestresult + "\"");
            }

            // Tant qu'il reste des mots à chercher, ou des résultats à traiter
            if ((linda.tryRead(new Tuple(Code.Result, reqUUID, String.class, Integer.class)) != null)
                    || (linda.tryRead(new Tuple(Code.Value, String.class, reqUUID)) != null)
                    || (linda.tryRead(new Tuple(Code.Request, reqUUID, String.class)) != null)) {

                linda.eventRegister(
                        Linda.eventMode.TAKE, Linda.eventTiming.IMMEDIATE,
                        new Tuple(Code.Result, reqUUID, String.class, Integer.class),
                        this);
            } else {
                System.out.println("Ending callback loop");
                linda.write(new Tuple(Code.Searcher, "end", reqUUID));
            }
        }
    }

    class TimeoutExit implements Runnable {
        @Override
        public void run() {
            try {
                Thread.sleep(timeout);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("Search (" + reqUUID + ") timed out ");
            linda.take(new Tuple(Code.Request, reqUUID, String.class)); // remove search query
            linda.takeAll(new Tuple(Code.Value, String.class, reqUUID)); // remove words

            new CbLoop(Linda.eventMode.TAKE, Linda.eventTiming.IMMEDIATE,
                    new Tuple(Code.Result, reqUUID, String.class, Integer.class)); // remove results

            new CbLoop(Linda.eventMode.TAKE, Linda.eventTiming.IMMEDIATE,
                    new Tuple(Code.Searcher, "done", reqUUID)); // remove "done" messages

            new CbLoop(Linda.eventMode.TAKE, Linda.eventTiming.IMMEDIATE,
                    new Tuple(Code.Searcher, "end", reqUUID)); // remove "end" messages

            // TODO: chercheur doivent écrire qui ils cherchent, pour que le manager puisse
            // se stopper su personne ne le cherche
        }
    }

    public void run() {
        if (this.timeout != null) {
            TimeoutExit exit = new TimeoutExit();
            Thread thread = new Thread(exit);
            thread.start();
        }

        this.loadData(); // on charge les mots du dict
        this.addSearch(); // on enregistre la query

        this.linda.eventRegister( // on attend la fin de la recherche
                eventMode.TAKE, eventTiming.IMMEDIATE,
                new Tuple(Code.Searcher, "done", this.reqUUID),
                new CbEnd());

        // on affiche le résultat (ou on le récupère...)
        linda.take(new Tuple(Code.Searcher, "end", this.reqUUID));
        System.out.println("Final result: \"" + bestresult + '"');
        linda.debug("Tuple Space : ");
    }
}
