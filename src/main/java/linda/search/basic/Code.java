package linda.search.basic;

public enum Code {
    Request, // Request, UUID, String
    Value, // Value, String, UUID
    Result, // Result, UUID, String, Int
    Searcher, // Result, "done", UUID
}
