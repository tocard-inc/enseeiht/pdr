package linda.search.basic;

import linda.*;

public class Main {

    private static final int NB_SEARCHER = 2;

    public static void main(String args[]) throws InterruptedException {
        // if (args.length != 2) {
        // System.err.println("linda.search.basic.Main search file.");
        // return;
        // }
        Linda linda = new linda.shm.CentralizedLinda();

        Manager manager1 = new Manager(linda, "/usr/share/dict/french", "abasourdir", 500);
        Thread thread1 = new Thread(manager1);

        Manager manager2 = new Manager(linda, "/usr/share/dict/french", "agneau");
        Thread thread2 = new Thread(manager2);

        for (int i = 0; i < NB_SEARCHER; i++) {
            Searcher searcher = new Searcher(linda);
            (new Thread(searcher)).start();
        }

        thread1.start();
        Thread.sleep(100);
        thread2.start();
        thread1.join();
        thread2.join();
    }
}