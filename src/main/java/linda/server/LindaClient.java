package linda.server;

import linda.Callback;
import linda.Linda;
import linda.Tuple;

import java.net.MalformedURLException;
import java.rmi.*;
import java.util.Collection;

/**
 * Client part of a client/server implementation of Linda.
 * It implements the Linda interface and propagates everything to the server it
 * is connected to.
 */
public class LindaClient implements Linda {

    LindaRemote lindaServer;

    /**
     * Initializes the Linda implementation.
     * 
     * @param serverURI the URI of the server, e.g.
     *                  "rmi://localhost:4000/LindaServer" or
     *                  "//localhost:4000/LindaServer".
     */
    public LindaClient(String serverURI) {
        try {
            this.lindaServer = (LindaRemote) Naming.lookup(serverURI);
        } catch (MalformedURLException | RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }

    public void write(Tuple t) {
        try {
            lindaServer.write(t);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public Tuple take(Tuple template) {
        try {
            return lindaServer.take(template);
        } catch (RemoteException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Tuple read(Tuple template) {
        try {
            return lindaServer.read(template);
        } catch (RemoteException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Tuple tryTake(Tuple template) {
        try {
            return lindaServer.tryTake(template);
        } catch (RemoteException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Tuple tryRead(Tuple template) {
        try {
            return lindaServer.tryRead(template);
        } catch (RemoteException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection<Tuple> takeAll(Tuple template) {
        try {
            return lindaServer.takeAll(template);
        } catch (RemoteException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection<Tuple> readAll(Tuple template) {
        try {
            return lindaServer.readAll(template);
        } catch (RemoteException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void eventRegister(eventMode mode, eventTiming timing, Tuple template, Callback callback) {
        new Thread() {
            public void run() {
                try {
                    callback.call(lindaServer.waitForTuple(mode, timing, template));
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public void debug(String prefix) {
        try {
            lindaServer.debug(prefix);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
