package linda.server;

import java.util.Collection;

import java.rmi.server.UnicastRemoteObject;
import java.rmi.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import linda.Tuple;
import linda.Linda.eventMode;
import linda.Linda.eventTiming;
import linda.shm.CentralizedLinda;

/**
 * lindaServer
 */
public class LindaServer extends UnicastRemoteObject implements LindaRemote {

    CentralizedLinda lindaInstance;
    Registry registry;
    LindaServer ls;

    public LindaServer() throws RemoteException {
        this.lindaInstance = new CentralizedLinda();
    }

    public LindaServer(int port) throws RemoteException {
        String url = "rmi://localhost:" + port + "/linda";

        try {
            this.ls = new LindaServer();
            this.registry = LocateRegistry.createRegistry(port);
            Naming.rebind(url, this.ls);
            System.out.println("L'instance Linda a été publiée sur le registre (" + url + ") !");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String... args) throws RemoteException {
        Integer port = 4000;
        if (args.length == 2)
            port = Integer.parseInt(args[1]);

        new LindaServer(port);
    }

    public void stop() throws AccessException, RemoteException, NotBoundException {
        this.registry.unbind("linda");
        UnicastRemoteObject.unexportObject(this.ls, true);
        UnicastRemoteObject.unexportObject(this.registry, true);
    }

    public void write(Tuple t) throws RemoteException {
        System.out.println("received write request: " + t);
        lindaInstance.write(t);
    }

    public Tuple take(Tuple template) throws RemoteException {
        Tuple res = lindaInstance.take(template);
        System.out.println("received take request, with template: " + template + "\nreturning: " + res);
        return res;
    }

    public Tuple read(Tuple template) throws RemoteException {
        Tuple res = lindaInstance.read(template);
        System.out.println("received read request, with template: " + template + "\nreturning: " + res);
        return res;
    }

    public Tuple tryTake(Tuple template) throws RemoteException {
        return lindaInstance.tryTake(template);
    }

    public Tuple tryRead(Tuple template) throws RemoteException {
        return lindaInstance.tryRead(template);
    }

    public Collection<Tuple> takeAll(Tuple template) throws RemoteException {
        return lindaInstance.takeAll(template);
    }

    public Collection<Tuple> readAll(Tuple template) throws RemoteException {
        return lindaInstance.readAll(template);
    }

    public Tuple waitForTuple(eventMode mode, eventTiming timing, Tuple template) throws RemoteException {
        return lindaInstance.waitForTuple(mode, timing, template);
    }

    public void debug(String prefix) throws RemoteException {
        lindaInstance.debug(prefix);
    }

}