package linda.server;

import linda.Linda;
import linda.Tuple;

import java.util.Collection;

import java.rmi.*;

public interface LindaRemote extends Remote {

    public void write(Tuple t) throws RemoteException;

    public Tuple take(Tuple template) throws RemoteException;

    public Tuple read(Tuple template) throws RemoteException;

    public Tuple tryTake(Tuple template) throws RemoteException;

    public Tuple tryRead(Tuple template) throws RemoteException;

    public Collection<Tuple> takeAll(Tuple template) throws RemoteException;

    public Collection<Tuple> readAll(Tuple template) throws RemoteException;

    public Tuple waitForTuple(Linda.eventMode mode, Linda.eventTiming timing, Tuple template) throws RemoteException;

    public void debug(String prefix) throws RemoteException;
}
