package linda.shm;

import linda.Callback;
import linda.Linda;
import linda.Tuple;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Lock;

class Reveil {
    Lock lock = new ReentrantLock();
    Condition condition;
    Tuple template;

    Reveil(Tuple template) {
        this.template = template;
        this.condition = lock.newCondition();
    }

    void sleep() {
        try {
            lock.lock();
            condition.await();
            lock.unlock();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    void reveil(Tuple t) {
        if (t.matches(this.template)) {
            lock.lock();
            condition.signal();
            lock.unlock();
        }
    }
}

/** Shared memory implementation of Linda. */
public class CentralizedLinda implements Linda {

    List<Tuple> tuples;
    List<Reveil> reveils;

    public CentralizedLinda() {
        this.tuples = Collections.synchronizedList(new ArrayList<Tuple>());
        this.reveils = Collections.synchronizedList(new ArrayList<Reveil>());
    }

    /**
     * Insert a tuple into the tuple list. (non-blocking)
     * 
     * @param template the Tuple template your looking for
     */
    public void write(Tuple t) {
        tuples.add(t);
        reveils.forEach(r -> r.reveil(t));
    }

    /**
     * Extract the tuple from the tuple list. (blocking)
     * 
     * @param template the Tuple template your looking for
     */
    public Tuple take(Tuple template) {
        Reveil reveil = new Reveil(template);
        reveils.add(reveil);

        Tuple result = null;

        result = tryTake(template);
        while (result == null) {
            reveil.sleep();
            result = tryTake(template);
        }

        return result;
    }

    /**
     * Get the tuple from the tuple list. (blocking)
     * 
     * @param template the Tuple template your looking for
     */
    public Tuple read(Tuple template) {
        Reveil reveil = new Reveil(template);
        reveils.add(reveil);

        Tuple result = null;

        result = tryRead(template);
        while (result == null) {
            reveil.sleep();
            result = tryRead(template);
        }

        return result;
    }

    /**
     * Extract the tuple from the tuple list. (non-blocking)
     * 
     * @param template the Tuple template your looking for
     */
    public Tuple tryTake(Tuple template) {
        synchronized (tuples) {
            Iterator<Tuple> iterator = tuples.iterator();
            Tuple next = null;

            while (iterator.hasNext()) {
                next = iterator.next();
                if (next.matches(template)) {
                    iterator.remove();
                    return next;
                }
            }
        }
        return null;
    }

    /**
     * Extract the tuple from the symetric difference of the Tuple-Space and the
     * given tuple list. (non-blocking)
     * 
     * @param template the Tuple template your looking for
     * @param excluded the list of Tuples to exclude from the search
     */
    private Tuple tryTake(Tuple template, Collection<Tuple> excluded) {
        synchronized (tuples) {
            Iterator<Tuple> iterator = tuples.iterator();
            Tuple next = null;

            while (iterator.hasNext()) {
                next = iterator.next();
                if (next.matches(template) && !excluded.contains(next)) {
                    iterator.remove();
                    return next;
                }
            }
        }
        return null;
    }

    /**
     * Get the tuple from the tuple list. (non-blocking)
     * 
     * @param template the Tuple template your looking for
     */
    public Tuple tryRead(Tuple template) {
        synchronized (tuples) {
            Tuple next = null;
            Iterator<Tuple> iterator = tuples.iterator();

            while (iterator.hasNext()) {
                next = iterator.next();
                if (next.matches(template)) {
                    return next;
                }
            }
        }
        return null;
    }

    /**
     * Get the tuple from the symetric difference of the Tuple-Space and the given
     * tuple list. (non-blocking)
     * 
     * @param template the Tuple template your looking for
     * @param excluded the list of Tuples to exclude from the search
     */
    private Tuple tryRead(Tuple template, Collection<Tuple> excluded) {
        synchronized (tuples) {
            Iterator<Tuple> iterator = tuples.iterator();
            Tuple next = null;

            while (iterator.hasNext()) {
                next = iterator.next();
                if (next.matches(template) && !excluded.contains(next)) {
                    return next;
                }
            }
        }
        return null;
    }

    /**
     * Extract the tuples in the tuple list. (non-blocking)
     * 
     * @param template the Tuple template your looking for
     */
    public Collection<Tuple> takeAll(Tuple template) {
        List<Tuple> results = new ArrayList<Tuple>();
        synchronized (tuples) {
            Iterator<Tuple> iterator = tuples.iterator();
            Tuple next = null;

            while (iterator.hasNext()) {
                next = iterator.next();
                if (next.matches(template)) {
                    results.add(next);
                    iterator.remove();
                }
            }
        }
        return results;
    }

    /**
     * Extract the tuples in the tuple list. (blocking)
     * 
     * @param template the Tuple template your looking for
     */
    public Collection<Tuple> readAll(Tuple template) {
        Collection<Tuple> results = new ArrayList<Tuple>();
        synchronized (tuples) {
            Iterator<Tuple> iterator = tuples.iterator();
            Tuple next = null;

            while (iterator.hasNext()) {
                next = iterator.next();
                if (next.matches(template)) {
                    results.add(next);
                }
            }
        }
        return results;
    }

    public Tuple waitForTuple(eventMode mode, eventTiming timing, Tuple template) {
        switch (timing) {
            case IMMEDIATE:
                return mode == eventMode.READ ? read(template) : take(template);
            case FUTURE:
                Collection<Tuple> knownTuples = new ArrayList<Tuple>();
                synchronized (tuples) {
                    for (Tuple t : tuples) {
                        knownTuples.add((Tuple) t.clone());
                    }
                }
                return future_search(template, knownTuples, mode);
            default:
                return null; // n'arrive jamais
        }
    }

    public void eventRegister(eventMode mode, eventTiming timing, Tuple template, Callback callback) {
        new Thread() {
            public void run() {
                callback.call(waitForTuple(mode, timing, template));
            }
        }.start();
    }

    private Tuple future_search(Tuple template, Collection<Tuple> knownTuples, eventMode mode) {
        Tuple result = null;

        Reveil reveil = new Reveil(template);
        reveils.add(reveil);

        result = mode == eventMode.READ ? tryRead(template, knownTuples) : tryTake(template, knownTuples);
        while (result == null) {
            reveil.sleep();
            result = mode == eventMode.READ ? tryRead(template, knownTuples) : tryTake(template, knownTuples);
        }
        return result;
    }

    public void debug(String prefix) {
        System.out.println(prefix + tuples.size());
    }

}
