package linda.eratosthene;

import linda.Tuple;
import linda.server.LindaClient;

public class Client {

    public static void main(String[] args) {
        LindaClient lc = new LindaClient(args[0]);

        int K = (int) lc.read(new Tuple(Integer.class)).get(0);
        System.out.println("K = " + K);

        while (true) {
            Tuple tuple_test = lc.tryTake(new Tuple("prime", "untested", Integer.class));
            System.out.println(tuple_test);
            if (tuple_test == null) {
                break;
            }
            int val = (int) tuple_test.get(2);
            tuple_test.set(1, "tested");
            lc.write(tuple_test);

            int iter = 2;
            while (iter * val < K) {
                Tuple tuple_testPrime = lc.take(new Tuple(String.class, String.class, iter*val));
                tuple_testPrime.set(0, "notprime");
                tuple_testPrime.set(1, "tested");
                lc.write(tuple_testPrime);
                iter++;
            }
        }

    }

}
