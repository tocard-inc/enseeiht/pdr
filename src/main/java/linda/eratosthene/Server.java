package linda.eratosthene;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.ArrayList;

import linda.Tuple;
import linda.server.LindaServer;

public class Server {

    public static void main(String[] args) {
        try {

            LindaServer ls = new LindaServer();

            int K = Integer.parseInt(args[2]);

            ls.write(new Tuple(K));

            for (int i = 2; i < K; i++) {
                ls.write(new Tuple("prime", "untested", i));
            }

            LocateRegistry.createRegistry(Integer.parseInt(args[1]));
            Naming.rebind(args[0], ls);
            System.out.println("L'instance Linda a été publié sur le registre (" + args[0] + ") !\n");

            while (null != ls.tryRead(new Tuple(String.class, "untested", Integer.class))) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            ArrayList<Tuple> ts = (ArrayList<Tuple>) ls.readAll(new Tuple("prime", String.class, Integer.class));
            Tuple primes = new Tuple();
            for (int i = 0; i < ts.size(); i++) {
                primes.add((int) ts.get(i).get(2));
            }

            System.out.println(primes);

        } catch (RemoteException | MalformedURLException e) {
            e.printStackTrace();
        }
    }
    
}
