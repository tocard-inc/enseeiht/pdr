package linda.test;

import linda.server.LindaClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;

import linda.Callback;
import linda.Tuple;
import linda.Linda.eventTiming;
import linda.Linda.eventMode;

public class REPL implements Callback, Serializable {

    public static class MyCallback implements Callback {
        public void call(Tuple t) {
            System.out.println("Tuple de l'event register : " + t);
        }
    }

    public static void main(String[] args) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        LindaClient lc = new LindaClient(args[0]);

        while (true) {
            afficherCommande();
            lireCommande(reader, lc);
        }

    }

    static void afficherCommande() {
        System.out.println("====================");
        System.out.println("Commandes :");
        System.out.println("w : write tuple");
        System.out.println("r : read tuple");
        System.out.println("t : take tuple");
        System.out.println("tr : tryRead tuple");
        System.out.println("tt : tryTake tuple");
        System.out.println("ra : readAll tuple");
        System.out.println("ta : takeAll tuple");
        System.out.println("evRI : eventRegister Read Immediate");
        System.out.println("evRF : eventRegister Read Future");
        System.out.println("evTI : eventRegister Take Immediate");
        System.out.println("evTF : eventRegister Take Future");
        System.out.println("\nPour les templates :");
        System.out.println("I : int");
        System.out.println("S : string");
        System.out.print("--------------------\n> ");
    }

    static void lireCommande(BufferedReader reader, LindaClient lc) {
        try {
            String ligne = reader.readLine();
            String[] args = ligne.split(" ");

            if (args.length < 2) {
                System.out.println("Il faut indiquer la commande et un tuple ! Ex : > r 1 S\n");
            } else {

                Serializable[] objets = new Serializable[args.length - 1];

                for (int i = 1; i < args.length; i++) {
                    if (args[i].equals("I")) {
                        objets[i - 1] = Integer.class;
                    } else if (args[i].equals("S")) {
                        objets[i - 1] = String.class;
                    } else {
                        try {
                            objets[i - 1] = Integer.parseInt(args[i]);
                        } catch (NumberFormatException e) {
                            objets[i - 1] = args[i];
                        }
                    }
                }

                Tuple t = new Tuple(objets);

                if (args[0].equals("w")) {
                    lc.write(t);
                    System.out.println("Tuple " + t + " écrit.");
                } else if (args[0].equals("r")) {
                    System.out.println("Attente de la lecture d'un tuple " + t + ".");
                    t = lc.read(t);
                    System.out.println("Tuple " + t + " lu.");
                } else if (args[0].equals("t")) {
                    System.out.println("Attente de la prise d'un tuple " + t + ".");
                    t = lc.take(t);
                    System.out.println("Tuple " + t + " pris.");
                } else if (args[0].equals("tr")) {
                    t = lc.tryRead(t);
                    System.out.println("Tuple " + t + " lu.");
                } else if (args[0].equals("tt")) {
                    t = lc.tryTake(t);
                    System.out.println("Tuple " + t + " pris.");
                } else if (args[0].equals("ra")) {
                    ArrayList<Tuple> ts = (ArrayList<Tuple>) lc.readAll(t);
                    System.out.print("Tuples ");
                    for (Tuple ti : ts) {
                        System.out.print(ti + " ");
                    }
                    System.out.println(" lus.");
                } else if (args[0].equals("ta")) {
                    ArrayList<Tuple> ts = (ArrayList<Tuple>) lc.takeAll(t);
                    System.out.print("Tuples ");
                    for (Tuple ti : ts) {
                        System.out.print(ti + " ");
                    }
                    System.out.println(" pris.");
                } else if (args[0].equals("evRI")) {
                    lc.eventRegister(eventMode.READ, eventTiming.IMMEDIATE, t, new MyCallback());
                } else if (args[0].equals("evRF")) {
                    lc.eventRegister(eventMode.READ, eventTiming.FUTURE, t, new MyCallback());
                } else if (args[0].equals("evTI")) {
                    lc.eventRegister(eventMode.TAKE, eventTiming.IMMEDIATE, t, new MyCallback());
                } else if (args[0].equals("evTF")) {
                    lc.eventRegister(eventMode.TAKE, eventTiming.FUTURE, t, new MyCallback());
                } else {
                    System.out.println("Il faut indiquer la commande et un tuple ! Ex : > r 1 S\n");
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void call(Tuple t) {
        System.out.println("Tuple " + t + " lu");
    }

}
