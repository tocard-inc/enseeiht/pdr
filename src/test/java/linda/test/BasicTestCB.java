
package linda.test;

import java.io.Serializable;

import linda.*;
import linda.Linda.eventMode;
import linda.Linda.eventTiming;

public class BasicTestCB implements Serializable {

    private static Linda linda;
    private static Tuple cbmotif;

    private static class MyCallback implements Callback, Serializable {
        public void call(Tuple t) {
            System.out.println("CB got " + t);
            linda.eventRegister(eventMode.TAKE, eventTiming.IMMEDIATE, cbmotif, this);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
            System.out.println("CB done with " + t);
        }
    }

    public static void main(String[] a) {
        // linda = new linda.shm.CentralizedLinda();
        linda = new linda.server.LindaClient("rmi://localhost:4000/linda");

        cbmotif = new Tuple(Integer.class, String.class);

        linda.debug("(1.1)");
        Tuple t1 = new Tuple(1, "a");
        System.out.println("(1) write: " + t1);
        linda.write(t1);
        linda.debug("(1.2)");

        linda.eventRegister(eventMode.READ, eventTiming.FUTURE, cbmotif, new MyCallback());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
        }

        linda.debug("(2.1)");
        Tuple t2 = new Tuple(2, "b");
        System.out.println("(2) write: " + t2);
        linda.write(t2);
        linda.debug("(2.2)");

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
        }

        linda.debug("(3.1)");
        Tuple t3 = new Tuple(3, "c");
        System.out.println("(3) write: " + t3);
        linda.write(t3);
        linda.debug("(3.2)");

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
        }

    }

}
