package linda.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import linda.Callback;
import linda.Linda;
import linda.Tuple;
import linda.Linda.eventMode;
import linda.Linda.eventTiming;
import linda.server.LindaClient;
import linda.server.LindaServer;

public class ClientServerTests {

    LindaServer ls;
    Linda linda;

    @BeforeEach
    void setUp() throws RemoteException, InterruptedException {
        ls = new LindaServer(4000);
        Thread.sleep(100);
        linda = new LindaClient("rmi://localhost:4000/linda");
    }

    @AfterEach
    void cleanup() throws AccessException, RemoteException, NotBoundException, InterruptedException {
        ls.stop();
        Thread.sleep(500);
    }

    @Nested
    @DisplayName("read related tests")
    class ReadTests {

        @Test
        void testTryRead() {
            Tuple tuple_write, tuple_template, tuple_read;

            tuple_template = new Tuple(0);
            tuple_read = linda.tryRead(tuple_template);
            assertEquals(tuple_read, null);

            tuple_template = new Tuple(Integer.class);
            tuple_read = linda.tryRead(tuple_template);
            assertEquals(tuple_read, null);

            tuple_write = new Tuple(0);
            linda.write(tuple_write);
            linda.write(tuple_write);

            tuple_template = new Tuple(0);
            tuple_read = linda.tryRead(tuple_template);
            assertEquals(tuple_read, tuple_write);

            tuple_template = new Tuple(Integer.class);
            tuple_read = linda.tryRead(tuple_template);
            assertEquals(tuple_read, tuple_write);
        }

        @Test
        void testRead() {
            Tuple tuple_write, tuple_template, tuple_read;

            tuple_write = new Tuple(0);
            linda.write(tuple_write);

            tuple_template = new Tuple(0);
            tuple_read = linda.read(tuple_template);
            assertEquals(tuple_read, tuple_write);

            tuple_template = new Tuple(Integer.class);
            tuple_read = linda.read(tuple_template);
            assertEquals(tuple_read, tuple_write);
        }

        @Test
        void testEmptyRead() {
            Thread mainRunner = new Thread(() -> {
                Tuple tuple_template;
                tuple_template = new Tuple(Integer.class);
                linda.read(tuple_template);
            });

            mainRunner.start();

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                assertEquals(Thread.State.WAITING, mainRunner.getState());
                mainRunner.interrupt();
            }
        }

        @Test
        void testWaitingRead() throws InterruptedException {
            Tuple tuple_write = new Tuple(0);

            Thread runnerExact = new Thread(() -> {
                Tuple tuple_template = new Tuple(Integer.class);
                Tuple tuple_read = linda.read(tuple_template);
                assertEquals(tuple_read, tuple_write);
            });
            runnerExact.start();

            Thread runnerType = new Thread(() -> {
                Tuple tuple_template = new Tuple(Integer.class);
                Tuple tuple_read = linda.read(tuple_template);
                assertEquals(tuple_read, tuple_write);
            });
            runnerType.start();

            Thread.sleep(1000);
            linda.write(tuple_write);
        }

        @Test
        void testReadAll() {
            Tuple tuple_template;
            Collection<Tuple> tuples_write, tuples_read, tuples_expected;

            tuples_write = new ArrayList<Tuple>();
            tuples_expected = new ArrayList<Tuple>();

            tuples_write.add(new Tuple(0));
            tuples_expected.add(new Tuple(0));

            tuples_write.add(new Tuple(1));
            tuples_expected.add(new Tuple(1));

            tuples_write.add(new Tuple(2));
            tuples_expected.add(new Tuple(2));

            tuples_write.add(new Tuple("3"));

            tuples_write.add(new Tuple("4"));

            tuples_write.add(new Tuple(5));
            tuples_expected.add(new Tuple(5));

            for (Tuple tuple : tuples_write) {
                linda.write(tuple);
            }

            tuple_template = new Tuple(Integer.class);
            tuples_read = linda.readAll(tuple_template);
            assertEquals(tuples_expected, tuples_read);

            assertNotEquals(linda.tryRead(tuple_template), null);
            assertNotEquals(linda.tryRead(new Tuple(String.class)), null);
        }
    }

    @Nested
    @DisplayName("Take related tests")
    class TakeTests {

        @Test
        void testTryTake() {
            Tuple tuple_write, tuple_template, tuple_read;

            tuple_template = new Tuple(0);
            tuple_read = linda.tryTake(tuple_template);
            assertEquals(tuple_read, null);

            tuple_template = new Tuple(Integer.class);
            tuple_read = linda.tryTake(tuple_template);
            assertEquals(tuple_read, null);

            tuple_write = new Tuple(0);
            linda.write(tuple_write);
            linda.write(tuple_write);

            tuple_template = new Tuple(0);
            tuple_read = linda.tryTake(tuple_template);
            assertEquals(tuple_read, tuple_write);

            tuple_template = new Tuple(Integer.class);
            tuple_read = linda.tryTake(tuple_template);
            assertEquals(tuple_read, tuple_write);
        }

        @Test
        void testWaitingTake() throws InterruptedException {
            Tuple tuple_write = new Tuple(0);

            Thread runnerExact = new Thread(() -> {
                Tuple tuple_template = new Tuple(Integer.class);
                Tuple tuple_read = linda.take(tuple_template);
                assertEquals(tuple_read, tuple_write);
            });
            runnerExact.start();

            Thread runnerType = new Thread(() -> {
                Tuple tuple_template = new Tuple(Integer.class);
                Tuple tuple_read = linda.take(tuple_template);
                assertEquals(tuple_read, tuple_write);
            });
            runnerType.start();

            Thread.sleep(500);
            linda.write(tuple_write);
            Thread.sleep(500);
            linda.write(tuple_write);
        }

        @Test
        void testEmptyTake() {
            Thread mainRunner = new Thread(() -> {
                Tuple tuple_template;
                tuple_template = new Tuple(Integer.class);
                linda.take(tuple_template);
            });

            mainRunner.start();

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                assertEquals(Thread.State.WAITING, mainRunner.getState());
                mainRunner.interrupt();
            }
        }

        @Test
        void testTake() {
            Tuple tuple_write, tuple_template, tuple_read;

            tuple_write = new Tuple(0);
            linda.write(tuple_write);
            linda.write(tuple_write);

            tuple_template = new Tuple(0);
            tuple_read = linda.take(tuple_template);
            assertEquals(tuple_read, tuple_write);

            tuple_template = new Tuple(Integer.class);
            tuple_read = linda.take(tuple_template);
            assertEquals(tuple_read, tuple_write);
        }

        @Test
        void testTakeAll() {
            Tuple tuple_template;
            Collection<Tuple> tuples_write, tuples_read, tuples_expected;

            tuples_write = new ArrayList<Tuple>();
            tuples_expected = new ArrayList<Tuple>();

            tuples_write.add(new Tuple(0));
            tuples_expected.add(new Tuple(0));

            tuples_write.add(new Tuple(1));
            tuples_expected.add(new Tuple(1));

            tuples_write.add(new Tuple(2));
            tuples_expected.add(new Tuple(2));

            tuples_write.add(new Tuple("3"));

            tuples_write.add(new Tuple("4"));

            tuples_write.add(new Tuple(5));
            tuples_expected.add(new Tuple(5));

            for (Tuple tuple : tuples_write) {
                linda.write(tuple);
            }

            tuple_template = new Tuple(Integer.class);
            tuples_read = linda.takeAll(tuple_template);
            assertEquals(tuples_expected, tuples_read);

            assertEquals(linda.tryTake(tuple_template), null);
            assertNotEquals(linda.tryTake(new Tuple(String.class)), null);
        }
    }

    @Nested
    @DisplayName("eventRegister related tests")
    class EventRegisterTests {

        final Tuple tuple_write = new Tuple(0);

        Callback assertCallback = new Callback() {
            @Override
            public void call(Tuple t) {
                assertEquals(t, tuple_write);
            }
        };

        @Test
        void IMMEDIATE_READ_test() throws InterruptedException {
            Thread runnerExact = new Thread(() -> {
                Tuple tuple_template = new Tuple(0);
                linda.eventRegister(
                        eventMode.READ,
                        eventTiming.IMMEDIATE,
                        tuple_template,
                        assertCallback);
            });
            runnerExact.start();

            Thread runnerType = new Thread(() -> {
                Tuple tuple_template = new Tuple(Integer.class);
                linda.eventRegister(
                        eventMode.READ,
                        eventTiming.IMMEDIATE,
                        tuple_template,
                        assertCallback);
            });
            runnerType.start();

            Thread.sleep(1000);
            linda.write(tuple_write);
        }

        @Test
        void IMMEDIATE_TAKE_test() throws InterruptedException {
            linda.write(new Tuple("999"));

            Thread runnerExact = new Thread(() -> {
                Tuple tuple_template = new Tuple(0);
                linda.eventRegister(
                        eventMode.TAKE,
                        eventTiming.IMMEDIATE,
                        tuple_template,
                        assertCallback);
            });
            runnerExact.start();

            Thread runnerType = new Thread(() -> {
                Tuple tuple_template = new Tuple(Integer.class);
                linda.eventRegister(
                        eventMode.TAKE,
                        eventTiming.IMMEDIATE,
                        tuple_template,
                        assertCallback);
            });
            runnerType.start();

            Thread.sleep(500);
            linda.write(tuple_write);
            Thread.sleep(500);
            linda.write(tuple_write);
        }

        @Test
        void FUTURE_READ_test() throws InterruptedException {
            linda.write(new Tuple(999));
            linda.write(new Tuple("999"));

            Thread runnerExact = new Thread(() -> {
                Tuple tuple_template = new Tuple(0);
                linda.eventRegister(
                        eventMode.READ,
                        eventTiming.FUTURE,
                        tuple_template,
                        assertCallback);
            });
            runnerExact.start();

            Thread runnerType = new Thread(() -> {
                Tuple tuple_template = new Tuple(Integer.class);
                linda.eventRegister(
                        eventMode.READ,
                        eventTiming.FUTURE,
                        tuple_template,
                        assertCallback);
            });
            runnerType.start();

            Thread.sleep(1000);
            linda.write(tuple_write);
        }

        @Test
        void FUTURE_TAKE_test() throws InterruptedException {
            linda.write(new Tuple(999));
            linda.write(new Tuple("999"));

            Thread runnerExact = new Thread(() -> {
                Tuple tuple_template = new Tuple(0);
                linda.eventRegister(
                        eventMode.TAKE,
                        eventTiming.FUTURE,
                        tuple_template,
                        assertCallback);
            });
            runnerExact.start();

            Thread runnerType = new Thread(() -> {
                Tuple tuple_template = new Tuple(Integer.class);
                linda.eventRegister(
                        eventMode.TAKE,
                        eventTiming.FUTURE,
                        tuple_template,
                        assertCallback);
            });
            runnerType.start();

            Thread.sleep(500);
            linda.write(tuple_write);
            Thread.sleep(500);
            linda.write(tuple_write);
        }
    }
}