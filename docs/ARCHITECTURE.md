# Architecture de principe

Fainsin Laurent - 2SN M2 \
Guillotin Damien - 2SN M2

## Consignes

Ce qu'il faut rendre
Un plan de travail initial par groupe.
Ce plan de travail doit lister les tâches à réaliser pour le groupe, et indiquer pour chacune des tâches, la personne affectée à la tâche
L'architecture de principe de la plateforme Linda à réaliser. Ce document ne devrait pas excéder 1 à 2 page(s) A4.

Il devrait comporter :
les principales classes envisagées
les difficultés identifiées
au besoin, des diagrammes de séquence à la UML, pour détailler les protocoles un peu complexes.
le type et l'organisation des tests envisagés

Note : le nom de l'archive à deposer doit être AV1_+ le nom du correspondant du groupe

## Plan de travail inital

### Version en mémoire partagée

Création de l'espace partagé de données typées Linda centralisé.
Il faut dans un premier temps implémenter un jeu de primitives spécifiques (les méthodes de l'interface).
Les primitives `write`, `take`, `tryTake` et `takeAll` seront réalisées par Laurent.
Les primitives `eventRegister`, `read`, `tryRead` et `readAll` seront réalisées par Damien.
La difficulté principale de ce programme semble résider dans l'implémentation de `eventRegister`.
La pluspart des tests que nous écrirons pour cette version seront unitaires.

### Version client / mono-serveur

Cette version ne diffère pas beaucoup de la précédente, la seule différence se situera dans l'implémentation de l'interface RMI. Comme demandé dans la consigne, nous devrions réussir a créer le server à partir de la classe qui implémente la version centralisée.

### Application Eratosthène

Laurent se chargera de créer le programme permettant de calculer les nombres premiers inférieur à `K` en se basant sur la méthode du crible d'Eratosthène.

### Application Levenshtein

Damien sera en charge de la recherche approximative dans un fichier qui se basera sur la distance de Levenshtein.

## Les principales classes

### Version en mémoire partagée

Pour la version partagée, une seule classe sera nécessaire puisque l'on veut justement partager la mémoire de cette dernière.

### Version Client/Server

Pour cette version, deux classes seront nécéssaire : la classe Client et la classe Server. Ces deux classes seront liées a une interface qui servira à indiquer les différentes méthodes que devra implémenter le server ainsi que les méthodes disponible par RMI pour le client.

### Les applications

Une classe `Orchestre` (spécifique à l'algorithme choisi, exemple Eratosthène ou Levenshtein) qui étend `Server` et qui s'occupera de distribuer la charge de travail pour les applications (qui seront des sous-classes de `Client`).

Les tests seront écrits avec la librairie JUnit et permettront de vérifier la cohérence de toutes méthodes écrites.
